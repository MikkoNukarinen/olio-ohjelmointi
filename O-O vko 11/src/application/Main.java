package application;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.fxml.FXMLLoader;


public class Main extends Application {

	Point point = new Point ();
	Straight straight = new Straight ();
	Circle tempPoint = null;
	ShapeHandler sh;
	boolean pointExists;
	int streakCounter;

	@Override
	public void start(Stage primaryStage) {
		try {
			BorderPane root = (BorderPane)FXMLLoader.load(getClass().getResource("Main.fxml"));
			root.setStyle("-fx-background-image:url('" + getClass().getResource("Suomen-kartta.jpg").toExternalForm() + "')");
			Scene scene = new Scene(root,650,1000);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();

			scene.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
		        @Override public void handle(MouseEvent event) {

		        	sh = ShapeHandler.getShapeHandler();
		        	pointExists = false;

		        	for (Circle circle : sh.pointList) {

		        		if ((Math.abs (event.getSceneX() - circle.getCenterX()) <= circle.getRadius()) &&
	        				(Math.abs (event.getSceneY() - circle.getCenterY()) <= circle.getRadius())) {
		        			pointExists = true;
		        		}
		        	}

		        	if (pointExists) {
		        		streakCounter++;
		        		if (streakCounter >= 2) {
		        			Line line = straight.getLine(tempPoint.getCenterX(), tempPoint.getCenterY(), event.getSceneX(), event.getSceneY());
		        			System.out.println(tempPoint.getCenterX());
		        			System.out.println(tempPoint.getCenterY());
		        			System.out.println(event.getSceneX());
		        			System.out.println(event.getSceneY());
		        			line.setStrokeWidth(5);
		        			line.setFill(Color.ALICEBLUE);
		        			root.getChildren().add(line);
		        			streakCounter = 0;
		        		}
		        		else {
		        			tempPoint = new Circle (event.getSceneX(), event.getSceneY(), 10);
		        		}
		        		System.out.println("Olen piste");
		        	}

		        	else {
		        		streakCounter = 0;
		        		double circlePosX = event.getSceneX();
			        	double circlePosY = event.getSceneY();
			        	Circle circle = point.getCircle(circlePosX, circlePosY);
			        	circle.setFill(Color.BLUE);
			        	root.getChildren().add(circle);
			        	System.out.println("streak:" + streakCounter);
		        	}
		        }
			});

		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}
}
