package application;

public class Bottle {

	public String bottleName = "";
	public double bottleSize;
	public double bottlePrice;

	public Bottle (String name, double size, double price) {
		bottleName = name;
		bottleSize = size;
		bottlePrice = price;
	}

	public String getBottleName() {
		return bottleName;
	}

	public String toString () {
		String bottleInfo = bottleName + ", " + bottleSize + "l, " + bottlePrice + "€";
		return bottleInfo;
	}
}
