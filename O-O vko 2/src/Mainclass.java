import java.util.Scanner;

public class Mainclass {

	/**
	 * @param args
	 */

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String name;
		String sentence;
		Scanner scan = new Scanner (System.in);

		System.out.print("Anna koiralle nimi: ");
		name = scan.nextLine();

		Dog dog = new Dog(name);

		System.out.print("Mitä koira sanoo: ");
		sentence = scan.nextLine();
		if (sentence.isEmpty()){
			System.out.print("Much wow!");
		}
		else {
			dog.speak(sentence);
		}
		scan.close();
	}
}
