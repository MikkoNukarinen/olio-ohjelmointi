package application;

public class Item {

	protected String name;
	protected boolean isBreakable;
	// Size in dm3
	protected float size;
	// Weight in kg
	protected float weight;

	public Item (String inName, boolean breaks, float inSize, float inWeight) {
		name = inName;
		isBreakable = breaks;
		size = inSize;
		weight = inWeight;
	}
}

class DancePants extends Item {
	public DancePants () {
		super("DancePants",false, 1f, 0.5f);
	}
}

class MoominMug extends Item {
	public MoominMug () {
		super("MoominMug", true, 0.5f, 1f);
	}
}

class GlassVase extends Item {
	public GlassVase () {
		super("GlassVase", true, 8f, 5f);
	}
}

class BagOfFireWood extends Item {
	public BagOfFireWood () {
		super("BagOfFireWood", false, 60f, 20f);
	}
}