
public abstract class Character {
	public WeaponBehaviour weapon;

	public void fight() {
		System.out.print(getClass().getName() + " tappelee aseella ");
		weapon.useWeapon();
	}
}

class King extends Character {
	public King() {

	}
}

class Knight extends Character {
	public Knight() {

	}
}

class Queen extends Character {
	public Queen() {

	}
}

class Troll extends Character {
	public Troll() {

	}
}

abstract class WeaponBehaviour {

	String name;

	public void useWeapon(){
		System.out.println(name);
	}
}

class KnifeBehaviour extends WeaponBehaviour {
	public KnifeBehaviour() {
		name = "Knife";
	}
}

class AxeBehaviour extends WeaponBehaviour {
	public AxeBehaviour() {
		name = "Axe";
	}
}

class SwordBehaviour extends WeaponBehaviour {
	public SwordBehaviour() {
		name = "Sword";
	}
}

class ClubBehaviour extends WeaponBehaviour {
	public ClubBehaviour() {
		name = "Club";
	}
}