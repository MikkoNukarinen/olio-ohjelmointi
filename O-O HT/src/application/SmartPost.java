package application;

public class SmartPost {

	private String code;
	private String city;
	private String address;
	private String availability;
	private String postOffice;
	private GeoLocation geoLocation;


	public SmartPost (String co, String ci, String ad, String av, String po, float la, float lo){

		code = co;
		city = ci;
		address = ad;
		availability = av;
		postOffice = po;
		geoLocation = new GeoLocation(la, lo);
	}

	public class GeoLocation {

		private float latitude;
		private float longitude;

		public GeoLocation(float la, float lo) {

			latitude = la;
			longitude = lo;
		}

		public float getLatitude () {
			return latitude;
		}

		public float getLongitude() {
			return longitude;
		}
	}

	public String getCity () {
		return city;
	}

	public String getAddress() {
		return address;
	}

	public String getAvailability () {
		return availability;
	}

	public String getCode() {
		return code;
	}

	public String getPostOffice () {
		return postOffice;
	}

	public GeoLocation getGeoLocation () {
		return geoLocation;
	}

}
