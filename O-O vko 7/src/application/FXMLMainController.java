package application;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class FXMLMainController {

    @FXML
    private Button button;

    @FXML
    private Label label;

    @FXML
    private TextField textField;

    @FXML
    void handleButtonAction(ActionEvent event) {
    	System.out.println("Hello World!");
    }

    @FXML
    void handleButtonAction2 (ActionEvent event) {
    	label.setText("Hello World!");
    }

    @FXML
    void handleButtonAction3 (ActionEvent event) {
    	label.setText(textField.getText());
    }

    @FXML
    void setText (ActionEvent event) {
    	label.setText(textField.getText());
    }
}
