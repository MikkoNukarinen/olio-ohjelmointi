package application;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;

import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

public class MainController {

	BottleDispenser bd = BottleDispenser.getBottleDispenser();
	DecimalFormat df = new DecimalFormat("#0.00");
	private ArrayList <Bottle> buyList = new ArrayList <Bottle>();

	@FXML
	private AnchorPane anchorPane;
	@FXML
	private Button addMoney;
	@FXML
	private Label label;
	@FXML
	private Label sodas;
	@FXML
	private Label money;
	@FXML
	private Label moneyOut;
	@FXML
	private TextField out;
	@FXML
	private Button buyBottle;
	@FXML
	private Button takeMoney;
	@FXML
	private Slider moneySlider;
	@FXML
	private ComboBox <Bottle> bottleListUI;

	@FXML
	void addMoney(ActionEvent event){
		double addedMoney = moneySlider.getValue();
		bd.addMoney(addedMoney);
		updateMoney();
		moneySlider.setValue(0);
	}

	@FXML
	void updateMoney (){
		String printMoney = df.format(bd.money);
		money.setText(printMoney + "€");
	}

	@FXML
	void listBottles(Event event){
		bottleListUI.getItems().clear();
		ArrayList <Bottle> bl = bd.getBottleList();
		for (Bottle bottle : bl) {
			bottleListUI.getItems().add(bottle);
		}
	}

	@FXML
	void buyBottle(ActionEvent event){
		Bottle bottle = bottleListUI.getValue();
		int buy = bd.buyBottle(bottle);
		switch (buy) {
		case 0:
			out.setText("No such sodas left :(");
			break;
		case 1:
			out.setText(bottle.bottleName + ", " + bottle.bottleSize + "l");
			buyList.add(bottle);
			break;
		case 2:
			out.setText("Not enough money!");
		}
		updateMoney();
	}

	@FXML
	void takeMoney(ActionEvent event) throws IOException{
		String printMoney = df.format(bd.money);
		moneyOut.setText(printMoney + "€");
		bd.returnMoney();
		updateMoney();
		bd.printReceipt(buyList);
	}
}
