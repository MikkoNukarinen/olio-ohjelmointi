import java.util.Scanner;
public class Dog {

	/**
	 * @param args
	 */
	private String name;
	private String says;

	Dog(String s){
		name = s.trim();
		if (name.isEmpty()){
			name = "Doge";
		}
		System.out.println("Hei, nimeni on " + name);

	}

	public void speak(String sentence) {
		Scanner scanType = new Scanner (sentence);
		while (scanType.hasNext()){
			if (scanType.hasNextInt()){
				System.out.print("Such integer: ");
			}
			else if (scanType.hasNextBoolean()){
				System.out.print("Such boolean: ");
			}
			says = scanType.next();
			System.out.println(says);
		}
	}
}
