import java.text.DecimalFormat;
import java.util.ArrayList;

public class BottleDispenser {

	private int bottleAmount;
	private float money;
	ArrayList <Bottle> bottleList;
	private int realIndex;
	String printMoney;

	static private BottleDispenser bd = null;

	static public BottleDispenser getBottleDispenser(){
		if (bd == null){
			bd = new BottleDispenser();
		}
		return bd;
	}

	private BottleDispenser() {
		money = 0;
		bottleList = new ArrayList <Bottle> ();
		Bottle bottle = new Bottle ("Pepsi Max", 0.5, 1.8);
		bottleList.add(bottle);
		Bottle bottle2 = new Bottle ("Pepsi Max", 1.5, 2.2);
		bottleList.add(bottle2);
		Bottle bottle3 = new Bottle ("Coca-Cola Zero", 0.5, 2.0);
		bottleList.add(bottle3);
		Bottle bottle4 = new Bottle ("Coca-Cola Zero", 1.5, 2.5);
		bottleList.add(bottle4);
		Bottle bottle5 = new Bottle ("Fanta Zero", 0.5, 1.95);
		bottleList.add(bottle5);
		Bottle bottle6 = new Bottle ("Fanta Zero", 0.5, 1.95);
		bottleList.add(bottle6);
		bottleAmount = bottleList.size();
	}

	public void addMoney() {
		money += 1;
		System.out.println("Klink! Lisää rahaa laitteeseen!");
	}

	public void buyBottle(int index) {
		realIndex = index - 1;
		if (bottleList.isEmpty()){
			System.out.println("Pullot loppu");
		}
		else {
			if (money <= bottleList.get(realIndex).bottlePrice){
				System.out.println("Syötä rahaa ensin!");
			}
			else {
				money -= bottleList.get(realIndex).bottlePrice;
				System.out.println("KACHUNK! " + bottleList.get(realIndex).bottleName +" tipahti masiinasta!");
				destroyBottle(realIndex);
			}
		}
	}

	public void returnMoney() {
		// huomhuom tässä on formatointia
		DecimalFormat decimalFormat=new DecimalFormat("#0.00");

		printMoney = decimalFormat.format(money).replace(".", ",");
		System.out.print("Klink klink. Sinne menivät rahat! Rahaa tuli ulos ");
		System.out.print(printMoney);
		System.out.println("€");
		money = 0;
	}

	public void listBottles() {
		for (int i = 0, k = 1; i < bottleList.size(); i++, k++){
			System.out.println(k + ". Nimi: " + bottleList.get(i).bottleName);
			System.out.println("	Koko: " + bottleList.get(i).bottleSize + "	Hinta: " + bottleList.get(i).bottlePrice);
		}
	}

	public void destroyBottle(int realIndex){
		bottleList.remove(realIndex);
	}
}
