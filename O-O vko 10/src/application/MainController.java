package application;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;

import javafx.scene.control.Button;

import javafx.scene.web.WebView;

import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

public class MainController {

	BrowserHistory bh = new BrowserHistory ();
	int previousClicks;
	int nextClicks;

	@FXML
	private Button previousPageButton;
	@FXML
	private Button nextPageButton;
	@FXML
	private TextField addressBar;
	@FXML
	private Button refreshButton;
	@FXML
	private Button commandButton;
	@FXML
	private WebView web;

	@FXML
	public void load (Event event) {
		event.getSource();

		String address = addressBar.getText();

		if (address.equals("index2.html")) {
			loadHTML();
		}
		else {
			web.getEngine().load("http://" + address);
			bh.addAddress(address);
		}
		previousClicks = 0;
		nextClicks = 0;
	}

	@FXML
	public void setAddress (MouseEvent event) {
		addressBar.setText("");
		previousClicks = 0;
		nextClicks = 0;
	}

	@FXML
	public void refreshPage (ActionEvent event) {
		String address = web.getEngine().getLocation();
		web.getEngine().load(address);
		previousClicks = 0;
		nextClicks = 0;
	}

	@FXML
	public void shoutOut (ActionEvent event) {
		web.getEngine().executeScript("document.shoutOut()");
		previousClicks = 0;
		nextClicks = 0;
	}

	@FXML
	public void initialize (ActionEvent event) {
		web.getEngine().executeScript("initialize()");
		previousClicks = 0;
		nextClicks = 0;
	}


	@FXML
	public void loadPreviousPage (ActionEvent event) {
		nextClicks = 0;
		String page = bh.getPreviousPage();
		if (previousClicks == 0) {
			page = bh.getPreviousPage();
		}
		web.getEngine().load("http://" + page);
		addressBar.setText(web.getEngine().getLocation());
		previousClicks ++;
	}

	@FXML
	public void loadNextPage (ActionEvent event) {
		previousClicks = 0;
		String page = bh.getNextPage();
		if (nextClicks == 0) {
			page = bh.getNextPage();
		}
		web.getEngine().load("http://" + page);
		addressBar.setText(web.getEngine().getLocation());
		nextClicks++;
	}


	public void loadHTML () {
		web.getEngine().load(getClass().getResource("index2.html").toExternalForm());
	}
}
