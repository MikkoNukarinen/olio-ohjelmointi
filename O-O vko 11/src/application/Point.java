package application;

import javafx.scene.shape.Circle;

public class Point {

	ShapeHandler sh = ShapeHandler.getShapeHandler();
	float radius = 10f;

	public Point () {

	}

	public Circle getCircle (double x, double y) {
		Circle circle = new Circle (x, y, radius);
		sh.addPoint(circle);
		return circle;
	}
}
