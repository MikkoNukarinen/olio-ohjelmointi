package application;

import javafx.scene.shape.Line;

public class Straight {

	ShapeHandler sh = ShapeHandler.getShapeHandler();

	public Straight () {

	}

	public Line getLine (double x1, double y1, double x2, double y2) {

		Line line = new Line ();
		line.setStartX(x1);
		line.setStartY(y1);
		line.setEndX(x2);
		line.setEndY(y2);

		sh.addLine(line);
		return line;
	}
}
