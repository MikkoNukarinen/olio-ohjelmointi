package application;

import java.util.ArrayList;

public class PostLister {

	private ArrayList <SmartPost> smartPostList = new ArrayList <SmartPost> ();

	// Since multiple classes utilize PostLister, it is made as a singleton to make the same ArrayList available for everyone
	static private PostLister ps = null;

	static public PostLister getPostLister() {

		if (ps == null){
			ps = new PostLister();
		}
		return ps;
	}

	private PostLister () {

	}

	public void addPostToList (SmartPost sp) {
		smartPostList.add(sp);
	}

	public ArrayList <SmartPost> getSmartPostList () {
		return smartPostList;
	}

	// Filter out the duplicates when there are multiple Smart Posts in one city
	public ArrayList <String> getCityList () {

		ArrayList <String> temp = new ArrayList <String> ();

		for (SmartPost sp : smartPostList) {
			if (! temp.contains(sp.getCity())){
				temp.add(sp.getCity());
			}
		}
		return temp;
	}

	// Return all the Smart Posts in a city
	public ArrayList <SmartPost> getSmartPostsForCity (String city) {

		ArrayList <SmartPost> temp = new ArrayList <SmartPost> ();

		for (SmartPost sp : smartPostList) {
			if (sp.getCity().equals(city)){
				temp.add(sp);
			}
		}
		return temp;
	}
}
