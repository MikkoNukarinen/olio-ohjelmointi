package application;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;

public class LogController implements Initializable{

	Log log = Log.getLog();

	@FXML
	private TextArea deliveryLog;


	@Override
	public void initialize(URL location, ResourceBundle resources) {

		String logInfo = log.getDeliveryLog();
		System.out.println("LogInfo: " + logInfo);
		deliveryLog.setText(logInfo);
	}
}
