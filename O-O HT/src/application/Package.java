package application;

import java.util.ArrayList;

public abstract class Package {

	// In addition to the item and package class information, Package class also contains the delivery route info

	protected int index;
	protected Item item;
	protected boolean breakableContent;
	// Maximum distance the package will travel in km
	protected float maxDistance;
	// Max size in dm3, from https://www.posti.fi/henkiloasiakkaat/apu-ja-tuki/hinnat.html
	protected float maxSize;
	// Max weight in kg
	protected static float maxWeight = 35f;
	protected int speed;
	// Coordinates will be used to draw the delivery route
	protected ArrayList <Float> coordinates;
	// Departure and destination cities will be used for log purposes
	protected String departureCity;
	protected String destinationCity;

	public Package (int inIndex, Item inItem, boolean breaks, float inDis, float inSize, int inSpeed, ArrayList <Float> inCoord, String depCity, String desCity){

		index = inIndex;
		item = inItem;
		breakableContent = breaks;
		maxDistance = inDis;
		maxSize = inSize;
		speed = inSpeed;
		coordinates = inCoord;
		departureCity = depCity;
		destinationCity = desCity;
	}


	@Override
	public String toString(){
		return this.item.name + ", " + this.departureCity + "-" + this.destinationCity;
	}
}

// Up to L-size package, fastest delivery, content breaks, range limited to 150km
class FirstClass extends Package {

	public static float firstClassMaxDistance = 150f;
	public static float firstClassMaxSize = 80f;
	public FirstClass (int inIndex, Item inItem, ArrayList <Float> inCoord, String depCity, String desCity) {
		super (inIndex, inItem, true, firstClassMaxDistance, firstClassMaxSize, 1, inCoord, depCity, desCity);
	}
}

// Up to S-size package, protects content, unlimited range
class SecondClass extends Package {

	public static float secondClassMaxSize = 24f;

	public SecondClass (int inIndex, Item inItem, ArrayList <Float> inCoord, String depCity, String desCity) {
		super (inIndex, inItem, false, 1500f, secondClassMaxSize, 2, inCoord, depCity, desCity);
	}
}

// Up to XL-size package, content breaks, slowest delivery
class ThirdClass extends Package {

	public static float thirdClassMaxSize = 130f;

	public ThirdClass (int inIndex, Item inItem, ArrayList <Float> inCoord, String depCity, String desCity) {
		super (inIndex, inItem, true, 1500f, thirdClassMaxSize, 3, inCoord, depCity, desCity);
	}
}
