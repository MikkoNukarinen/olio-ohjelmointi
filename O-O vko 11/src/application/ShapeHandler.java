package application;

import java.util.ArrayList;

import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

public class ShapeHandler {

	public ArrayList <Circle> pointList = new ArrayList <Circle> ();
	public ArrayList <Line> lineList = new ArrayList <Line> ();

	static private ShapeHandler sh = null;

	static public ShapeHandler getShapeHandler () {
		if (sh == null){
			sh = new ShapeHandler();
		}
		return sh;
	}

	private ShapeHandler () {

	}

	public void addPoint (Circle circle) {
		pointList.add(circle);
	}

	public void addLine(Line line) {
		lineList.add(line);
	}
}
