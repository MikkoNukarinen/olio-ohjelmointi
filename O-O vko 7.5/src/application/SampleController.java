package application;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;

import javafx.scene.control.Button;

import javafx.scene.control.TextArea;

public class SampleController {

	String textLine;
	Scanner scan = new Scanner(System.in);
	BufferedReader in;
	BufferedWriter out;

	@FXML
	private Button save;
	@FXML
	private Button load;
	@FXML
	private TextArea textArea;

	@FXML
	void readFromFile (ActionEvent event) throws FileNotFoundException, IOException {
		String inFile = getFilename();
		in = new BufferedReader (new FileReader (inFile));
		while (true) {
			textLine = in.readLine();
			if (textLine == null) {
				break;
			}
			else {
				textArea.setText(textArea.getText() + textLine);
			}
		}
		in.close();
	}

	String getFilename () {
		System.out.println("Anna tiedoston nimi: ");
		String filename = scan.next();
		return filename;
	}

	@FXML
	void writeToFile (ActionEvent event) throws IOException {
		String outFile = getFilename();
		String outputText = textArea.getText();
		out = new BufferedWriter (new FileWriter (outFile));
		out.write(outputText);
		out.close();
	}
}
