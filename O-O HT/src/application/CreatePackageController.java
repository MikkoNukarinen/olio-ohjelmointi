package application;

import javafx.fxml.FXML;

import javafx.scene.control.Button;

import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.control.Label;

import javafx.scene.control.ComboBox;

import javafx.scene.control.TextArea;

import javafx.scene.control.CheckBox;

import java.util.ArrayList;

import javafx.event.ActionEvent;
import javafx.event.Event;

public class CreatePackageController {

	private PostLister pl = PostLister.getPostLister();
	private Storage storage = Storage.getStorage();
	private Log log = Log.getLog();
	private int packageIndex = 0;


	@FXML
	private Label title;
	@FXML
	private ComboBox <String> premadeItems;
	@FXML
	private Label choosePremadeObjectText;
	@FXML
	private Label createNewObjectText;
	@FXML
	private TextField newItemName;
	@FXML
	private TextField newItemSize;
	@FXML
	private TextField newItemWeight;
	@FXML
	private CheckBox breakableChoice;
	@FXML
	private Label choosePackageClassText;
	@FXML
	private ComboBox <String> packageClasses;
	@FXML
	private TextArea packageClassDescription;
	@FXML
	private Label transportInfoTitle;
	@FXML
	private Label departureTitle;
	@FXML
	private ComboBox <String> departureCityChoice;
	@FXML
	private ComboBox <String> departureSmartPostChoice;
	@FXML
	private Label targetTitle;
	@FXML
	private ComboBox <String> destinationCityChoice;
	@FXML
	private ComboBox <String> destinationSmartPostChoice;
	@FXML
	private Button confirmPackageCreationButton;
	@FXML
	private Label itemErrorText;
	@FXML
	private Label deliveryErrorText;
	@FXML
	private Label packageCreatedText;


	@FXML
	private void showPremadeItems(Event event) {

		itemErrorText.setText("");
		premadeItems.getItems().clear();
		packageCreatedText.setText("");

		premadeItems.getItems().add("DancePants");
		premadeItems.getItems().add("MoominMug");
		premadeItems.getItems().add("GlassVase");
		premadeItems.getItems().add("BagOfFireWood");

		// If the user has started to build their own item, but chose to use premade one instead
		newItemName.setText("");
		newItemSize.setText("");
		newItemWeight.setText("");
	}

	@FXML
	private void showPackageClasses (Event event) {

		itemErrorText.setText("");
		packageClasses.getItems().clear();
		deliveryErrorText.setText("");
		packageCreatedText.setText("");

		packageClasses.getItems().add("FirstClass");
		packageClasses.getItems().add("SecondClass");
		packageClasses.getItems().add("ThirdClass");

	}

	// Clear each of the text input fields as user clicks them
	@FXML
	private void setText(MouseEvent event) {

		packageCreatedText.setText("");
		premadeItems.setValue(null);

		// Catch an error where user tries to create a new item with a premade one selected
		if (premadeItems.getValue() != null){
			setItemErrorText();
			return;
		}

		itemErrorText.setText("");
		TextField temp;
		temp = (TextField) event.getSource();
		temp.setText("");
	}

	@FXML
	private void setItemErrorText () {
		itemErrorText.setText("Either select a premade object, or create new one");
	}

	@FXML
	private void showCities (Event event) {

		deliveryErrorText.setText("");

		// Utilise temporary object to use this for either departure or destination city
		ComboBox <String> temp;
		temp = (ComboBox <String>) event.getSource();

		temp.getItems().clear();

		// Filter out the duplicates: only show each city once
		ArrayList <String> tempCityList = pl.getCityList();

		for (String city : tempCityList) {

			temp.getItems().add(city);
		}
	}

	@FXML
	private void showDepartureSmartPosts (Event event) {

		if (departureCityChoice.getValue() == null){

			deliveryErrorText.setText("Please select departure city first");
		}

		departureSmartPostChoice.getItems().clear();

		String city = departureCityChoice.getValue();

		// Based on the selected city, get the relevant Smart Posts
		ArrayList <SmartPost> tempSmartPostList = pl.getSmartPostsForCity(city);

		for (SmartPost sp : tempSmartPostList) {
			departureSmartPostChoice.getItems().add(sp.getPostOffice());
		}
	}

	@FXML
	private void showDestinationSmartPosts (Event event) {

		deliveryErrorText.setText("");

		if (destinationCityChoice.getValue() == null){

			deliveryErrorText.setText("Please select destination city first");
		}

		destinationSmartPostChoice.getItems().clear();

		String city = destinationCityChoice.getValue();

		// Based on the selected city, get the relevant Smart Posts
		ArrayList <SmartPost> tempSmartPostList = pl.getSmartPostsForCity(city);

		for (SmartPost sp : tempSmartPostList) {
			destinationSmartPostChoice.getItems().add(sp.getPostOffice());
		}
	}

	@FXML
	private void createPackage (ActionEvent event) {

		Item item = null;
		String name;
		String packageClass;
		Package newPackage;
		ArrayList <Float> coordinates;
		boolean packageCreated = false;

		// Check if the user desires to package a premade item
		if (premadeItems.getValue() != null) {

			name = premadeItems.getValue();

			switch (name) {

			case "DancePants":
				item = new DancePants();
				break;

			case "MoominMug":
				item = new MoominMug();
				break;

			case "GlassVase":
				item = new GlassVase();
				break;

			case "BagOfFireWood":
				item = new BagOfFireWood();
				break;
			}

		}

		// Otherwise, create a new item based on user input
		else {

			name = newItemName.getText();
			boolean breaks = breakableChoice.isSelected();
			float size = Float.parseFloat(newItemSize.getText());
			float weight = Float.parseFloat(newItemWeight.getText());

			item = new Item (name, breaks, size, weight);
		}

		// Check if the user picked a package class
		if (packageClasses.getValue() == null){
			itemErrorText.setText("Please select a package class");
			return;
		}

		else {
			packageClass = packageClasses.getValue();
		}


		// Max weight for Smart post regardless of the package class is 35kg
		if (item.weight > Package.maxWeight) {
			deliveryErrorText.setText("Item is too heavy for SmartPost transport, max weight: 35kg");
			return;
		}

		// Get the coordinates and city names for later use

		if (departureSmartPostChoice.getValue() == null){
			deliveryErrorText.setText("Please select departure Smart Post");
			return;
		}

		if (destinationSmartPostChoice.getValue() == null){
			deliveryErrorText.setText("Please select destination Smart Post");
			return;
		}

		coordinates = getCoordinates ();

		String departureCity = departureCityChoice.getValue();
		if (departureCity == null){
			deliveryErrorText.setText("Please select departure city");
			return;
		}

		String destinationCity = destinationCityChoice.getValue();
		if (destinationCity == null){
			deliveryErrorText.setText("Please select destination city");
			return;
		}

		switch (packageClass){

		case "FirstClass":

			if (item.size > FirstClass.firstClassMaxSize){
				deliveryErrorText.setText("Item too large for 1st class, please see class info");
				break;
			}

			else if (item.isBreakable){
				deliveryErrorText.setText("For breakable items, please use second class packages");
				break;
			}

			newPackage = new FirstClass (packageIndex, item, coordinates, departureCity, destinationCity);
			storage.addPackageToStorage(newPackage);
			packageCreated = true;
			break;

		case "SecondClass":

			if (item.size > SecondClass.secondClassMaxSize){
				deliveryErrorText.setText("Item too large for 2nd class, please see class info");
				break;
			}

			newPackage = new SecondClass (packageIndex, item, coordinates, departureCity, destinationCity);
			storage.addPackageToStorage(newPackage);
			packageCreated = true;
			break;

		case "ThirdClass":

			if (item.size > ThirdClass.thirdClassMaxSize) {
				deliveryErrorText.setText("Item is too large for SmartPost transport");
				break;
			}

			else if (item.isBreakable) {
				deliveryErrorText.setText("For breakable items, please use second class packages");
				break;
			}

			newPackage = new ThirdClass (packageIndex, item, coordinates, departureCity, destinationCity);
			storage.addPackageToStorage(newPackage);
			packageCreated = true;
			break;
		}

		if (packageCreated){
			packageCreatedText.setText("Package created");
			log.logPackageCreation(item, departureCity, destinationCity);
			packageIndex++;
		}
	}

	// Get the coordinates for the chosen start and end SmartPosts from their GeoLocation classes
	private ArrayList <Float> getCoordinates () {

		ArrayList <Float> temp = new ArrayList <Float>();
		String departure = departureSmartPostChoice.getValue();
		String destination = destinationSmartPostChoice.getValue();
		float departureLat = 0f;
		float departureLng = 0f;
		float destinationLat = 0f;
		float destinationLng = 0f;

		ArrayList <SmartPost> postList = pl.getSmartPostList();

		for (SmartPost sp : postList) {
			if (sp.getPostOffice().equals(departure)) {
				departureLat = sp.getGeoLocation().getLatitude();
				departureLng = sp.getGeoLocation().getLongitude();
			}

			else if (sp.getPostOffice().equals(destination)) {
				destinationLat = sp.getGeoLocation().getLatitude();
				destinationLng = sp.getGeoLocation().getLongitude();
			}
		}
		temp.add(departureLat);
		temp.add(departureLng);
		temp.add(destinationLat);
		temp.add(destinationLng);
		return temp;
	}
}
