// formatting copied from Stack Overflow
import java.util.Scanner;

public class Mainclass {

	public static void main(String[] args) {

		BottleDispenser bd = BottleDispenser.getBottleDispenser();
		Scanner scan = new Scanner(System.in);
		boolean quitProgram = false;

		do {
			System.out.println();
			System.out.println("*** LIMSA-AUTOMAATTI ***");
			System.out.println("1) Lisää rahaa koneeseen");
			System.out.println("2) Osta pullo");
			System.out.println("3) Ota rahat ulos");
			System.out.println("4) Listaa koneessa olevat pullot");
			System.out.println("0) Lopeta");
			System.out.print("Valintasi: ");
			int input = scan.nextInt();
			switch (input) {
			case 1:
				bd.addMoney();
				break;
			case 2:
				bd.listBottles();
				System.out.print("Valintasi: ");
				int bottleIndex = scan.nextInt();
				bd.buyBottle(bottleIndex);
				break;
			case 3:
				bd.returnMoney();
				break;
			case 4:
				bd.listBottles();
				break;
			case 0:
				quitProgram = true;
			}
		} while (quitProgram == false);

		}
	}
