package application;

import java.io.IOException;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.fxml.FXMLLoader;


public class Main extends Application {

	private Log log = Log.getLog();

	@Override
	public void start(Stage primaryStage) {

		try {
			BorderPane root = (BorderPane)FXMLLoader.load(getClass().getResource("Main.fxml"));
			Scene scene = new Scene(root,900,800);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
			primaryStage.setOnHiding( event -> {
				try {
					log.writeLogFile();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}} );

		} catch(Exception e) {
			e.printStackTrace();
		}

		MainController.packageCreation.setOnHiding(event -> {
			MainController.createPackageWindowOpen = false;
		});

		MainController.logWindow.setOnHiding(event -> {
			MainController.logWindowOpen = false;
		});

	}

	public static void main(String[] args) {
		launch(args);
	}
}
