import java.util.ArrayList;

public class Bank {

	Account account = null;
	ArrayList<Account> accountList = new ArrayList<Account>();

	public Bank() {
	}

	private Account getAccount (String accountID) {
		for (Account a : accountList) {
			if (a.id.matches(accountID)) {
				return a;
			}
		}
		return null;
	}

	public void createRegularAccount (String accountID, int accountBalance) {
		account = new RegularAccount(accountID, accountBalance);
		accountList.add(account);
		System.out.println("Tili luotu.");
	}

	public void createCreditAccount (String accountID, int accountBalance, int accountCredit) {
		account = new CreditAccount (accountID, accountBalance, accountCredit);
		accountList.add(account);
		System.out.println("Tili luotu.");
	}

	public void removeAccount (String accountID) {
		Account a = getAccount(accountID);
		accountList.remove(a);
		System.out.println("Tili poistettu.");
	}

	public void deposit (String accountID, int moneyAmount) {
		Account a = getAccount(accountID);
		a.deposit(moneyAmount);
	}

	public void withdraw (String accountID, int moneyAmount) {
		Account a = getAccount(accountID);
		a.withdraw(moneyAmount);
	}

	public void printAllAccounts () {
		System.out.println("Kaikki tilit:");
		for (Account a : accountList) {
			a.printInfo();
		}
	}

	public void printAccount (String accountID) {
		for (Account a : accountList) {
			a.printInfo();
		}
	}

}
