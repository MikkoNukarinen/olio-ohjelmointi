import java.util.Scanner;

public class Mainclass {

	public static void main(String[] args) {

		// periytyminen, voidaan luoda yleinen character, joka voidaan rakentaa new King()???
		Character character = null;
		Scanner scan = new Scanner(System.in);

		int input;
		boolean quitProgram = false;

		while (!quitProgram) {

			System.out.println("*** TAISTELUSIMULAATTORI ***");
			System.out.println("1) Luo hahmo");
			System.out.println("2) Taistele hahmolla");
			System.out.println("0) Lopeta");
			System.out.print("Valintasi: ");
			input = scan.nextInt();

			switch (input) {
			case 1:
				System.out.println("Valitse hahmosi: ");
				System.out.println("1) Kuningas");
				System.out.println("2) Ritari");
				System.out.println("3) Kuningatar");
				System.out.println("4) Peikko");
				System.out.print("Valintasi: ");
				input = scan.nextInt();

				switch (input) {
				case 1:
					character = new King();
					break;
				case 2:
					character = new Knight();
					break;
				case 3:
					character = new Queen();
					break;
				case 4:
					character = new Troll();
					break;
				}

				System.out.println("Valitse aseesi: ");
				System.out.println("1) Veitsi");
				System.out.println("2) Kirves");
				System.out.println("3) Miekka");
				System.out.println("4) Nuija");
				System.out.print("Valintasi: ");
				input = scan.nextInt();

				switch (input) {
				case 1:
					character.weapon = new KnifeBehaviour();
					break;
				case 2:
					character.weapon = new AxeBehaviour();
					break;
				case 3:
					character.weapon = new SwordBehaviour();
					break;
				case 4:
					character.weapon = new ClubBehaviour();
					break;
				}
				break;
			case 2:
				character.fight();
				break;
			case 0:
				scan.close();
				quitProgram = true;
			}
		}
	}
}
