package application;

import java.io.IOException;
import java.net.MalformedURLException;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.Map.Entry;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.AnchorPane;

public class MainController {

	TheaterManager tm = new TheaterManager ();
	HashMap <String, String> theaterMap;
	HashMap <String, HashMap<String, String>> showMap;

	@FXML
	private AnchorPane dateText;
	@FXML
	private ComboBox <String> theaterSelection;
	@FXML
	private Label headerText;
	@FXML
	private Label selectText;
	@FXML
	private TextField dateField;
	@FXML
	private Label timeText;
	@FXML
	private TextField startTimeField;
	@FXML
	private TextField endTimeField;
	@FXML
	private Button listMoviesButton;
	@FXML
	private Label nameSearchText;
	@FXML
	private TextField nameSearchField;
	@FXML
	private Button nameSearchButton;
	@FXML
	private ListView movieList;
	@FXML
	private Label errorText;

	@FXML
	private void getTheaters () throws MalformedURLException, IOException {
		String theaterXML = tm.getXMLString("http://www.finnkino.fi/xml/TheatreAreas/");

		tm.createMap(theaterXML, "theaters");

		theaterMap = tm.getMap("theaters");
	}

	@FXML
	private void showTheaters (Event event) throws MalformedURLException, IOException {
		getTheaters();
		theaterSelection.getItems().clear();

		for (Entry <String, String> e : theaterMap.entrySet()) {

			String id = e.getKey();
			String name = e.getValue();

			if (!id.equals("1029") && !id.equals("1014")){
				theaterSelection.getItems().add(name);
			}
		}
	}

	@FXML
	private void setText(MouseEvent event) {
		TextField temp;
		temp = (TextField) event.getSource();
		temp.setText("");
	}

	@FXML
	private void getShows () throws MalformedURLException, IOException {
		String searchStartString;
		String searchEndString;
		LocalTime searchStart;
		LocalTime searchEnd;

		movieList.getItems().clear();
		nameSearchField.setText("");
		errorText.setText("");

		String name = theaterSelection.getValue();
		String id = tm.getID(name);
		String date = dateField.getText();

		String showXML = tm.getXMLString("http://www.finnkino.fi/xml/Schedule/?area=" + id + "&dt=" + date);

		tm.createMap(showXML, "shows");

		showMap = tm.getMap("shows");

		for (Entry <String, HashMap <String, String>> e : showMap.entrySet()) {
			String title = e.getValue().get("Title");
			String startTimeString = e.getValue().get("Start time");
			String length = e.getValue().get("Length");

			String info = title + ", " + startTimeString + ", " + length;

			if (startTimeField.getText().isEmpty() || startTimeField.getText().equals("Alkamisaika")) {
				searchStartString = "00:00";
			}
			else {
				searchStartString = startTimeField.getText();
			}

			if (endTimeField.getText().isEmpty() || endTimeField.getText().equals("Päättymisaika")) {
				searchEndString = "23:59";
			}
			else {
				searchEndString = endTimeField.getText();
			}


			try {
				searchStart = LocalTime.parse(searchStartString);
			} catch (DateTimeParseException ex) {
				errorText.setText("Kirjoita alkamisaika muodossa: hh:mm");
			}

			try {
				searchEnd = LocalTime.parse(searchEndString);
			} catch (DateTimeParseException ex) {
				errorText.setText("Kirjoita alkamisaika muodossa: hh:mm");
			}


			LocalTime startTime = LocalTime.parse(startTimeString);
			// Miksei näitä voi poistaa?
			searchStart = LocalTime.parse(searchStartString);
			searchEnd = LocalTime.parse(searchEndString);

			if (startTime.isAfter(searchStart) && startTime.isBefore(searchEnd)) {
				movieList.getItems().add(info);
			}
		}
	}

	@FXML
	private void getTheatersByShow (ActionEvent event) throws MalformedURLException, IOException {
		String searchStartString;
		String searchEndString;
		LocalTime searchStart;
		LocalTime searchEnd;
		String date = dateField.getText();
		String searchName = nameSearchField.getText();

		movieList.getItems().clear();
		getTheaters();

		for (Entry <String, String> e : theaterMap.entrySet()) {

			String id = e.getKey();

			if (!id.equals("1029") && !id.equals("1014")) {

				String theater = e.getValue();

				String showXML = tm.getXMLString("http://www.finnkino.fi/xml/Schedule/?area=" + id + "&dt=" + date);

				tm.createMap(showXML, "shows");

				showMap = tm.getMap("shows");

				for (Entry <String, HashMap <String, String>> e2 : showMap.entrySet()) {
					String title = e2.getValue().get("Title");

					if (title.equals(searchName)) {
						String startTimeString = e2.getValue().get("Start time");
						String length = e2.getValue().get("Length");

						String info = theater + ", " + startTimeString + ", " + length;

						if (startTimeField.getText().isEmpty() || startTimeField.getText().equals("Alkamisaika")) {
							searchStartString = "00:00";
						}
						else {
							searchStartString = startTimeField.getText();
						}

						if (endTimeField.getText().isEmpty() || endTimeField.getText().equals("Päättymisaika")) {
							searchEndString = "23:59";
						}
						else {
							searchEndString = endTimeField.getText();
						}


						try {
							searchStart = LocalTime.parse(searchStartString);
						} catch (DateTimeParseException ex) {
							errorText.setText("Kirjoita alkamisaika muodossa: hh:mm");
						}

						try {
							searchEnd = LocalTime.parse(searchEndString);
						} catch (DateTimeParseException ex) {
							errorText.setText("Kirjoita alkamisaika muodossa: hh:mm");
						}

						LocalTime startTime = LocalTime.parse(startTimeString);
						searchStart = LocalTime.parse(searchStartString);
						searchEnd = LocalTime.parse(searchEndString);

						if (startTime.isAfter(searchStart) && startTime.isBefore(searchEnd)) {
							movieList.getItems().add(info);
						}
					}
				}
			}
		}
	}
}


