package application;

import java.util.ArrayList;

public class Storage {

	private ArrayList <Package> packageList = new ArrayList <Package>();

	// Only one storage will ever be used, so it is created as a Singleton
	static private Storage storage = null;

	static public Storage getStorage () {

		if (storage == null) {
			storage = new Storage();
		}
		return storage;
	}

	private Storage () {

	}

	public void addPackageToStorage (Package p) {
		if (p != null){
			packageList.add(p);
			System.out.println("Package added. Storage size: " + packageList.size());
		}
	}

	public ArrayList <Package> getPackageList () {
		return packageList;
	}

	// When the package is successfully sent, it is removed from storage
	public void removePackageFromStorage (Package p) {
		if (p != null) {
			System.out.println("Package removed from storage");
			packageList.remove(p);
		}
	}
}
