import java.util.Scanner;

public class Mainclass {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		Bank bank = new Bank ();

		boolean quitProgram = false;
		int input;
		String accountID;
		int moneyAmount;
		int creditAmount;

		while (!quitProgram) {
			System.out.println();
			System.out.println("*** PANKKIJÄRJESTELMÄ ***");
			System.out.println("1) Lisää tavallinen tili");
			System.out.println("2) Lisää luotollinen tili");
			System.out.println("3) Tallenna tilille rahaa");
			System.out.println("4) Nosta tililtä");
			System.out.println("5) Poista tili");
			System.out.println("6) Tulosta tili");
			System.out.println("7) Tulosta kaikki tilit");
			System.out.println("0) Lopeta");
			System.out.print("Valintasi: ");
			input = scan.nextInt();

			switch (input) {

			case 1:
				System.out.print("Syötä tilinumero: ");
				accountID = scan.next();
				System.out.print("Syötä rahamäärä: ");
				moneyAmount = scan.nextInt();
				bank.createRegularAccount(accountID, moneyAmount);
				break;
			case 2:
				System.out.print("Syötä tilinumero: ");
				accountID = scan.next();
				System.out.print("Syötä rahamäärä: ");
				moneyAmount = scan.nextInt();
				System.out.print("Syötä luottoraja: ");
				creditAmount = scan.nextInt();
				bank.createCreditAccount(accountID, moneyAmount, creditAmount);
				break;
			case 3:
				System.out.print("Syötä tilinumero: ");
				accountID = scan.next();
				System.out.print("Syötä rahamäärä: ");
				moneyAmount = scan.nextInt();
				bank.deposit(accountID, moneyAmount);
				break;
			case 4:
				System.out.print("Syötä tilinumero: ");
				accountID = scan.next();
				System.out.print("Syötä rahamäärä: ");
				moneyAmount = scan.nextInt();
				bank.withdraw(accountID, moneyAmount);
				break;
			case 5:
				System.out.print("Syötä poistettava tilinumero: ");
				accountID = scan.next();
				bank.removeAccount(accountID);
				break;
			case 6:
				System.out.print("Syötä tulostettava tilinumero: ");
				accountID = scan.next();
				bank.printAccount(accountID);
				break;
			case 7:
				bank.printAllAccounts();
				break;
			case 0:
				quitProgram = true;
				scan.close();
				break;
			default:
				System.out.println("Valinta ei kelpaa.");
			}
		}
	}

}
