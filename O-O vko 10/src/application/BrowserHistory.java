package application;

import java.util.ArrayList;
import java.util.ListIterator;

public class BrowserHistory {

	ArrayList <String> history;
	ListIterator <String> li;

	public BrowserHistory () {
		history = new ArrayList <String> ();
		li = history.listIterator();
	}

	public void addAddress (String address) {
		li.add(address);
	}

	public String getPreviousPage () {
		if (li.hasPrevious()) {
			String previous = li.previous();
			return previous;
		}
		return null;
	}

	public String getNextPage () {
		if (li.hasNext()) {
			String next = li.next();
			return next;
		}
		return null;
	}
}
