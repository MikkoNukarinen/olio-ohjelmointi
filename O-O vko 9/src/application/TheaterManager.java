package application;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class TheaterManager {

	private Document doc;
	private HashMap <String, String> theaterMap;
	private HashMap <String, HashMap <String, String>> showMap;

	public HashMap getMap (String tag) {

		switch (tag) {

		case "theaters":
			return theaterMap;

		case "shows":
			return showMap;
		}

		return null;
	}

	public TheaterManager () {

	}

	public String getXMLString (String address) throws MalformedURLException, IOException {

		URL url = new URL (address);

		BufferedReader br = new BufferedReader (new InputStreamReader (url.openStream()));

		String XMLString = "";
		String line;

		while ((line = br.readLine())!= null) {
			XMLString += line + "\n";
		}
		br.close();
		return XMLString;
	}

	public void createMap (String XMLString, String tag) {

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

			doc = dBuilder.parse(new InputSource (new StringReader (XMLString)));
			doc.getDocumentElement().normalize();

		} catch (ParserConfigurationException e) {
			System.out.println("Parser Configuration Exception");
			e.printStackTrace();
		} catch (SAXException e) {
			System.out.println("SAX Exception");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("IO Exception");
			e.printStackTrace();
		}

		NodeList nodes;

		switch (tag) {

		case "theaters":
			nodes = getNodeList ("TheatreArea");
			theaterMap = new HashMap <String, String> ();
			theaterMap = fillMap (nodes, theaterMap, "theaters");
			break;

		case "shows":
			nodes = getNodeList ("Show");
			showMap = new HashMap <String, HashMap <String,String>> ();
			showMap = fillMap (nodes, showMap, "shows");
			break;
		}

	}

	private NodeList getNodeList (String tag) {
		NodeList nl = doc.getElementsByTagName(tag);
		return nl;
	}

	private HashMap fillMap (NodeList nodes, HashMap map, String tag){
		for (int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);
			Element e = (Element) node;

			switch (tag) {

			case "theaters":
				map.put(getValue ("ID", e), getValue("Name", e));
				break;


			case "shows":
				HashMap <String, String> temp = new HashMap <String, String> ();

				int lengthInMinutes = Integer.parseInt(getValue ("LengthInMinutes", e));
				int hours = lengthInMinutes / 60;
				int minutes = lengthInMinutes % 60;

				String length = Integer.toString(hours) + "h " + Integer.toString(minutes) + " min";

				String input = getValue ("dttmShowStart", e);

				DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
				LocalTime time = LocalTime.parse(input, formatter);
				String startTime = time.toString();

				temp.put("Title" ,getValue ("Title", e));
				temp.put("Start time", startTime);
				temp.put("Length", length);

				map.put(getValue ("ID", e), temp);
				break;
			}
		}
		return map;
	}

	public void parseShows (String id, String date) {
		System.out.println("http://www.finnkino.fi/xml/Schedule/?area=" + id + "&dt=" + date);
	}

	private String getValue(String tag, Element e){
		return ( e.getElementsByTagName(tag).item(0).getTextContent());
	}


	public String getID (String inName) {
		for (Entry <String, String> e : theaterMap.entrySet()){
			if (e.getValue().equals(inName)){
				return e.getKey();
			}
		}
		return null;
	}
}


