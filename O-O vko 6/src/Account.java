
public abstract class Account {

	protected String id;
	protected int balance;

	public Account (String inID, int inBalance) {
		id = inID;
		balance = inBalance;
	}

	public void withdraw (int moneyAmount) {
		if (balance < moneyAmount) {
			System.out.println("Tilillä ei tarpeeksi rahaa.");
		}
		else {
			balance -= moneyAmount;
		}
	}

	public void deposit (int moneyAmount) {
		balance += moneyAmount;
	}

	public void printInfo () {
		System.out.println("Tilinumero: " + id + " Tilillä rahaa: " + balance);
	}
}

class RegularAccount extends Account {
	public RegularAccount (String inID, int inBalance) {
		super(inID, inBalance);
	}
}

class CreditAccount extends Account {
	private int credit;

	public CreditAccount (String inID, int inBalance, int inCredit) {
		super(inID, inBalance);
		credit = inCredit;
	}

	public void withdraw (int moneyAmount) {
		if ((balance + credit) < moneyAmount) {
			System.out.println("Tilillä ei tarpeeksi rahaa.");
		}
		else {
			balance -= moneyAmount;
		}
	}

	public void printInfo () {
		System.out.println("Tilinumero: " + id + " Tilillä rahaa: " + balance + " Luottoraja: " + credit);
	}
}