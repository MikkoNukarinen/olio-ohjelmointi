package application;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Log {

	// Log class updates the delivery log, and writes the log file when the software closes

	private String deliveryInfo = "";
	private int sentPackages = 0;
	private Storage storage = Storage.getStorage();

	static private Log log = null;

	static public Log getLog () {

		if (log == null){
			log = new Log ();
		}
		return log;
	}

	private Log () {

	}

	public void logDeliveryInfo (String inInfo, double inDistance) {

		sentPackages++;
		deliveryInfo += "Delivery number: " + sentPackages + "\n" + inInfo + "\nDistance: " + Double.toString(inDistance) + "km\n\n";
		System.out.println("Delivery info logged");
	}

	public void logPackageCreation (Item inItem, String depCity, String desCity) {

		deliveryInfo += "Package created \nItem: " + inItem.name + "\n" + "Route: " + depCity + "-" + desCity + "\n\n";
		System.out.println("Package creation logged");
	}

	public String getDeliveryLog () {
		return deliveryInfo;
	}

	public void writeLogFile () throws IOException {
		BufferedWriter bw = new BufferedWriter (new FileWriter ("Log.txt"));
		String logText = log.getDeliveryLog();
		ArrayList <Package> storageList = storage.getPackageList();

		bw.write("### LOG ###" + "\n\n\n");
		bw.write("STORAGE \n");
		bw.write("Packages in storage: " + storageList.size() + "\n\n");
		for (Package p : storageList){
			bw.write("Item: " + p.item.name + "\n");
			bw.write("Route: " + p.departureCity + "-" + p.destinationCity + "\n\n");
		}
		bw.write("DELIVERIES \n");
		bw.write(logText);
		bw.close();
	}

}
