
public class Car {

	private String model;
	private int year;
	private int mileage;
	private int topSpeed;
	private float acceleration;
	private Body body;
	private Chassis chassis;
	private Engine engine;
	private Wheels wheels;

	public Car () {
		body = new Body();
		chassis = new Chassis();
		engine = new Engine();
		wheels = new Wheels();
	}

	public void print() {
		System.out.println("Autoon kuuluu:");
		System.out.println("	Body");
		System.out.println("	Chassis");
		System.out.println("	Engine");
		System.out.println("	" + this.wheels.number + " Wheel");
	}

	public class Body {
		private String type;
		private int numberOfDoors;
		private String colour;

		public Body() {
			System.out.println("Valmistetaan: Body");
		}
	}

	public class Chassis {
		private String driveTrain;
		private String transmission;
		private int numberOfGears;

		public Chassis() {
			System.out.println("Valmistetaan: Chassis");
		}
	}

	public class Engine {
		private int size;
		private int power;
		private int torgue;
		private String fuel;
		private float fuelEconomy;

		public Engine() {
			System.out.println("Valmistetaan: Engine");
		}
	}



	public class Wheels {
		private String size;
		private int number = 4;

		public Wheels() {
			for (int i = 0; i < number; i++) {
				System.out.println("Valmistetaan: Wheel");
			}
		}
	}
}
