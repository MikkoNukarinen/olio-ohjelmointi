package application;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class DataBuilder {

	private String XMLString;
	private Document doc;
	private NodeList nodes;

	public DataBuilder () {

	}

	PostLister pl = PostLister.getPostLister();

	// Read the Smart Post data and return it as a string
	public void getSmartPostXML () throws IOException {

		URL url = new URL ("http://smartpost.ee/fi_apt.xml");

		BufferedReader br = new BufferedReader (new InputStreamReader (url.openStream()));

		XMLString = "";
		String line;

		// As long as the line isn't empty, it is added to the string
		while ((line = br.readLine()) != null) {
			XMLString += line + "\n";
		}

		br.close();
		parseData();
	}

	// Parse the XML string and create a NodeList, that can be used to create Smart Post objects
	private void parseData () {

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

			doc = dBuilder.parse(new InputSource (new StringReader (XMLString)));
			doc.getDocumentElement().normalize();

		} catch (ParserConfigurationException e) {
			System.out.println("Parser Configuration Exception");
			e.printStackTrace();

		} catch (SAXException e) {
			System.out.println("SAX Exception");
			e.printStackTrace();

		} catch (IOException e) {
			System.out.println("IO Exception");
			e.printStackTrace();
		}

		nodes = doc.getElementsByTagName("place");
		createSmartPosts();

	}

	// Create SmartPost objects and add them to an ArrayList
	private void createSmartPosts () {

		String code;
		String city;
		String address;
		String availability;
		String postOffice;
		float latitude;
		float longitude;

		for (int i = 0; i < nodes.getLength(); i++){

			SmartPost sp;
			Node node = nodes.item(i);
			Element e = (Element) node;

			// Go through each attribute of each smart post, use those to create the objects, and add them to the list
			code = e.getElementsByTagName("code").item(0).getTextContent();
			city = e.getElementsByTagName("city").item(0).getTextContent();
			address = e.getElementsByTagName("address").item(0).getTextContent();
			availability = e.getElementsByTagName("availability").item(0).getTextContent();
			postOffice = e.getElementsByTagName("postoffice").item(0).getTextContent();
			latitude = Float.parseFloat(e.getElementsByTagName("lat").item(0).getTextContent());
			longitude = Float.parseFloat(e.getElementsByTagName("lng").item(0).getTextContent());

			sp = new SmartPost (code, city, address, availability, postOffice, latitude, longitude);
			pl.addPostToList(sp);
		}
	}
}
