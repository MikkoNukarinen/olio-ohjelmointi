import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

public class ReadAndWriteIO {

	String inFile;
	String textLine;

	public ReadAndWriteIO() {

	}

	public void readAndWrite() throws IOException {

		ZipInputStream zipin = new ZipInputStream (new FileInputStream ("zipinput.zip"));
		ZipEntry zipEntry = zipin.getNextEntry();
		inFile = zipEntry.getName();
		BufferedReader in = new BufferedReader (new InputStreamReader (zipin));
		while (true) {
			if ((textLine = in.readLine()) == null) {
				break;
			}
			else {
				System.out.println(textLine);
			}
		}
	}
}
