package application;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class BottleDispenser {

	public double money;
	ArrayList <Bottle> bottleList;
	String printMoney;

	static private BottleDispenser bd = null;

	static public BottleDispenser getBottleDispenser(){
		if (bd == null){
			bd = new BottleDispenser();
		}
		return bd;
	}

	private BottleDispenser() {
		money = 0;
		bottleList = new ArrayList <Bottle> ();
		Bottle bottle = new Bottle ("Pepsi Max", 0.5, 1.8);
		bottleList.add(bottle);
		Bottle bottle2 = new Bottle ("Pepsi Max", 1.5, 2.2);
		bottleList.add(bottle2);
		Bottle bottle3 = new Bottle ("Coca-Cola Zero", 0.5, 2.0);
		bottleList.add(bottle3);
		Bottle bottle4 = new Bottle ("Coca-Cola Zero", 1.5, 2.5);
		bottleList.add(bottle4);
		Bottle bottle5 = new Bottle ("Fanta Zero", 0.5, 1.95);
		bottleList.add(bottle5);
		Bottle bottle6 = new Bottle ("Fanta Zero", 0.5, 1.95);
		bottleList.add(bottle6);
	}

	public ArrayList <Bottle> getBottleList () {
		return bottleList;
	}

	public void addMoney(double addedMoney) {
		money += addedMoney;
	}

	public int buyBottle (Bottle purchase) {
		for (Bottle bottle : bottleList) {
			if (bottle.bottleName.equals(purchase.bottleName) && bottle.bottleSize == purchase.bottleSize) {
				if (money < bottle.bottlePrice){
					return 2;
				}
				else {
					money -= bottle.bottlePrice;
					destroyBottle(bottleList.indexOf(bottle));
					return 1;
				}
			}
		}
		return 0;
	}

	public void returnMoney() {
		money = 0;
	}

	public void destroyBottle(int realIndex){
		bottleList.remove(realIndex);
	}

	public void printReceipt (ArrayList<Bottle> buyList) throws IOException {

		BufferedWriter bw = new BufferedWriter (new FileWriter ("Receipt.txt"));
		bw.write("### RECEIPT ###" + '\n');
		for (Bottle bottle : buyList) {
			bw.write(bottle.bottleName + ", " + bottle.bottleSize + "l" + '\n');
		}
		bw.write("THANK YOU FOR YOUR PURCHASE!");
		bw.close();
	}
}
