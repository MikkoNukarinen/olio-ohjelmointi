package application;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;


import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

public class MainController implements Initializable{

	private DataBuilder db = new DataBuilder ();
	private PostLister pl = PostLister.getPostLister();
	private Storage storage = Storage.getStorage();
	private Log log = Log.getLog();

	private ArrayList <SmartPost> smartPostList;
	private ArrayList <String> tempCityList;

	static public Stage packageCreation = new Stage();
	static public boolean createPackageWindowOpen = false;
	static public Stage logWindow = new Stage();
	static public boolean logWindowOpen = false;


	public MainController () {

	}

	@FXML
	private WebView webView;

	@FXML
	private ComboBox <String> cityList;

	@FXML
	private Button addSmartPosts;

	@FXML
    private Button createPackage;

	@FXML
	private ComboBox <Package> packageSelection;

	@FXML
	private Button sendButton;

	@FXML
	private Button deleteRoutesButton;

	@FXML
	private Label deliveryErrorText;

	@FXML
	private Label packageInfoText;

	@FXML
	private Button showLogButton;

	@FXML
	private ComboBox <Integer> packageClassSelection;

	@FXML
	private Label packageInfoUpdatedText;

	// Load the map when starting the application
	@Override
	public void initialize (URL url, ResourceBundle rb) {
		webView.getEngine().load(getClass().getResource("index.html").toExternalForm());

		try {
			db.getSmartPostXML();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	private void showCities (Event event) {

		cityList.getItems().clear();

		// Filter out the duplicates: only show each city once
		tempCityList = pl.getCityList();

		for (String city : tempCityList){
			cityList.getItems().add(city);
		}
	}

	@FXML
	private void drawPosts (ActionEvent event) {

		String city = cityList.getValue();
		// Make error check here
		smartPostList = pl.getSmartPostList();

		for (SmartPost sp : smartPostList) {

			if (sp.getCity().equals(city)){
				String address = sp.getAddress() + ", " + sp.getCode() + " " + sp.getCity();
				String other = sp.getPostOffice() + " " + sp.getAvailability();
				String colour = "red";
				String parameter = "'" + address + " '" + ", " + "' " + other + "'" + ", " + "'" + colour + "'";
				webView.getEngine().executeScript("document.goToLocation(" + parameter + ")");
			}
		}
	}

	@FXML
	private void openCreatePackageWindow (ActionEvent event) {

		if (!createPackageWindowOpen) {
			packageCreation = new Stage ();
			createPackageWindowOpen = true;
		}

		try {
			Parent page = FXMLLoader.load(getClass().getResource("CreatePackage.fxml"));

			Scene scene = new Scene (page);

			packageCreation.setScene(scene);
			packageCreation.show();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	private void openLog (ActionEvent event) {

		if (!logWindowOpen) {
			logWindow = new Stage ();
			logWindowOpen = true;
		}

		try {
			Parent page = FXMLLoader.load(getClass().getResource("Log.fxml"));

			Scene scene = new Scene (page);

			logWindow.setScene(scene);
			logWindow.show();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	@FXML
	private void showStorage (Event event) {

		deliveryErrorText.setText("");
		packageSelection.getItems().clear();
		packageInfoText.setText("");
		packageInfoUpdatedText.setText("");

		ArrayList <Package> temp = storage.getPackageList();

		for (Package p : temp) {
			packageSelection.getItems().add(p);
		}
	}

	// When the user selects a package from the storage, show additional information about that package
	@FXML
	private void setPackageInfoText (Event event) {

		Package p = packageSelection.getValue();

		if (p == null) {
			return;
		}

		packageInfoText.setText("Item: " + p.item.name + "\n" + "Route: " + p.departureCity + "-" + p.destinationCity);
	}

	@FXML
	private void transportPackage (Event event) {

		if (packageSelection.getValue() == null) {
			deliveryErrorText.setText("Please select a package to be transported");
			return;
		}
		Package p = packageSelection.getValue();

		double distance = 0;


		try {
			distance = (double)webView.getEngine().executeScript("document.calculatePath(" + p.coordinates + " )");
		} catch (ClassCastException e) {
			System.out.println("Ei voi luoda double arvoa integeristä.");
			try {
				System.out.println("Yritetään parsia integeristä.");
				int dist = (Integer)webView.getEngine().executeScript("document.calculatePath(" + p.coordinates + " )");
				distance = Double.parseDouble(Integer.toString(dist));
			} catch (ClassCastException ex) {
				System.out.println("Ei voida varsia integeristä doublea.");
			}

		}


		if ((float)distance > p.maxDistance){

			// Only package class limited by range is the first
			deliveryErrorText.setText("Maximum distance for first class delivery is 150km, please change package class");
			return;
		}

		// If the package can be sent, remove it from storage
		else {

			System.out.println("Speed: " + p.speed);
			webView.getEngine().executeScript("document.createPath(" + p.coordinates + ", 'blue', " + p.speed + ")");
			packageSelection.setValue(null);

			storage.removePackageFromStorage(p);
			packageInfoUpdatedText.setText("Package sent");
			log.logDeliveryInfo(packageInfoText.getText(), distance);
		}
	}

	@FXML
	private void deleteExistingRoutes (ActionEvent event) {

		webView.getEngine().executeScript("document.deletePaths()");
	}

	@FXML
	private void showPackageClass (Event event) {

		packageClassSelection.getItems().clear();
		packageClassSelection.getItems().add(1);
		packageClassSelection.getItems().add(2);
		packageClassSelection.getItems().add(3);
	}

	// If the user wants to change the package class, remove the old package and create a new one
	@FXML
	private void setPackageClass (ActionEvent event) {

		boolean thisBreaks = false;

		if (packageSelection.getValue() == null){
			deliveryErrorText.setText("Select the package, whose class you wish to change");
			return;
		}

		Package old = packageSelection.getValue();
		if (old.item.isBreakable){
			thisBreaks = true;
		}

		int index = old.index;
		Item item = old.item;
		ArrayList <Float> coord = old.coordinates;
		String depCity = old.departureCity;
		String desCity = old.destinationCity;

		int packageClass = packageClassSelection.getValue();

		if (packageClass == old.speed){
			deliveryErrorText.setText("The package is already class " + packageClass);
			return;
		}

		Package p = null;

		System.out.println(thisBreaks);
		if (thisBreaks && packageClass!= 2){
			deliveryErrorText.setText("Only class 2 supports breakable items");
			return;
		}

		switch (packageClass){

		case 1:
			p = new FirstClass(index, item, coord, depCity, desCity);
			storage.removePackageFromStorage(old);
			storage.addPackageToStorage(p);
			break;
		case 2:
			p = new SecondClass(index, item, coord, depCity, desCity);
			storage.removePackageFromStorage(old);
			storage.addPackageToStorage(p);
			break;
		case 3:
			p = new ThirdClass(index, item, coord, depCity, desCity);
			storage.removePackageFromStorage(old);
			storage.addPackageToStorage(p);
			break;
		}

		packageInfoUpdatedText.setText("Package class updated");
		if (p != null){
			packageSelection.setValue(p);
			deliveryErrorText.setText("");
		}
	}
}
